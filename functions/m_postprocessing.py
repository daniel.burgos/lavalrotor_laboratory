# %% Functions for processing data
import os
import json
import numpy as np
from numpy.fft import rfft, fft
from typing import Dict, Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    vec_accel = []
    for ii in range(len(x)):
        vec_accel.append(np.sqrt(x[ii]**2+y[ii]**2+z[ii]**2))
        
    return(np.array(vec_accel))


def interpolation(time: np.ndarray, data: np.ndarray, int_time: np.ndarray) -> np.ndarray:
    """Linearly interpolates values in data.

    Args:
        time (ndarray): Timestamp of the values in data
        data (ndarray): Values to interpolate
        int_time (ndarray): Points in time at which the data is to be interpolated.

    Returns:
        int_data (ndarray): Interpolation points based on 'time'.
    """
    int_data = np.interp(int_time,time,data)
    
    return(int_data)

def my_fft_scaled(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x (with numpy fft() or rfft()) and scales the FFT amplitude.

    It is assumed that the time interval between the sampled sensor data
    is constant.

    Args:
        x (ndarray): Measurement data that are transformed into the
            frequency domain.
        time (ndarray): Timestamp of the measurement data

    Returns:
        (ndarray): Scaled Amplitude of the computed FFT spectrum
        (ndarray): Frequency of the computed FFT spectrum
    """
    # Mittelwert auf Null verschieben
    x = x-np.mean(x)
    

    X = np.fft.fft(x)
    N = len(X)
    sr = N/(np.max(time)-np.min(time))
    n = np.arange(N)
    T = N/sr
    freq = n/T
    

    n_oneside = N//2
    X_oneside = np.abs(X[:n_oneside])/N
    f_oneside = freq[:n_oneside]
    return([X_oneside,f_oneside])



def evaluate_measurement_metadata(folder_path_metadata: str) -> Dict:
    """Finds the path to the JSON setup file and generates the setup_dict from it.

    Args:
        folder_path_metadata (str): Path to the folder with all relevant JSON metadata files, including JSON setup file.

    Returns:
        dict: Information in the setup_json_path file, enriched with the paths to the JSON files of the components.
    """
    for dir_path, _, file_names in os.walk(folder_path_metadata):
        for file_name in file_names:
            if file_name.endswith((".json")):
                file_path = os.path.join(dir_path, file_name)
                with open(file_path, "r") as json_file:
                    json_content = json.load(json_file)
                    if "setup" in json_content.keys():
                        setup_json_path = file_path
    return evaluate_setup(setup_json_path, folder_path_metadata)


def evaluate_setup(setup_json_path: str, folder_path_metadata: str) -> Dict:
    """Scans all metadata specified in the file with the path equal to setup_json_path.

    Args:
        setup_json_path (str): Path to setup JSON file.
        folder_path_metadata (str): Path where the JSON files of the components are located.

    Raises:
        RuntimeError: Is triggered if the information in the component JSON file does not match the information
            in the JSON-Setup file.

    Returns:
        dict: Information in the setup_json_path file, enriched with the paths to the JSON files of the components.
    """
    with open(setup_json_path, "r") as json_file:
        json_content = json.load(json_file)

    setup_dict = json_content["setup"]

    for dir_path, _, file_names in os.walk(folder_path_metadata):
        for file_name in file_names:
            if file_name.endswith((".json")):
                file_path = os.path.join(dir_path, file_name)
                with open(file_path, "r") as json_file:
                    json_content = json.load(json_file)
                    json_file_uuid = json_content["JSON"]["ID"]
                if json_file_uuid in setup_dict.keys():
                    json_file_type = list(json_content.keys())[1]
                    if setup_dict[json_file_uuid]["type"] == json_file_type:
                        setup_dict[json_file_uuid]["path"] = file_path
                    else:
                        print(
                            "Something is wrong with {} or the corresponding entry in the JSON setup file.".format(
                                file_path
                            )
                        )
                        raise RuntimeError(
                            "Metadata in JSON files is not specified correctly."
                        )

    return setup_dict


def extract_uuid(setup_dict: Dict) -> Tuple[str, str]:
    """Extratcts uuid of measuring devices from setup_dict.

    Args:
        setup_dict (dict): Information in the setup_json_path file.

    Returns:
        id_accelerometer (str): Uuid of 'accelerometer', required to read the acceleration from the h5 file
        id_hall (str):  Uuid of 'motor_controller', required to read the motor-rpm from the h5 file.
    """
    id_accelerometer = None
    id_hall = None
    for component_dict, component_uuid in zip(setup_dict.values(), setup_dict.keys()):
        if "sensor" in component_dict["type"]:
            if component_dict["name"] == "accelerometer":
                if id_accelerometer is None:
                    id_accelerometer = component_uuid
                else:
                    print("Several 'acccelerometers' defined in the JSON setup file")

        elif "instrument" in component_dict["type"]:
            if component_dict["name"] == "motor_controller":
                if id_hall is None:
                    id_hall = component_uuid
                else:
                    print("Several 'motor_controller' defined in the JSON setup file")

    if (id_accelerometer is None) or (id_hall is None):
        raise RuntimeError(
            "Relevant information is missing in the JSON setup file."
        )
    return id_accelerometer, id_hall
